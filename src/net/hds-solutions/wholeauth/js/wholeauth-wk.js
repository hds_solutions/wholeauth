/**
 * WholeAuth SDK - Worker module
 * <p>
 * Copyrigth 2014 - HDS Solutions
 * </p>
 */
(function($, _, undefined) {
    // registramos el modulo
    var $this = _.registerModule('wk', '0.3.0', true);
    $this.init = function() {
        if ($().actual === undefined)
            throw '$.actual jQuery method does not exist';
        // creamos el worker
        var $worker = '<div class="wholeauth-worker">';
        $worker += '<div class="overlay"></div>';
        $worker += '<div class="content"></div>';
        $worker += '</div>';
        // agregamos el worker
        if (typeof $('.wholeauth-worker')[0] === 'undefined')
            $('#wholeauth-root').append($worker);
        // creamos el data loader
        var $dloader = '<div class="wholeauth-dataloader"><p>Loading<span></span>...</p></div>';
        // agregamos el data loader
        if (typeof $('.wholeauth-dataloader')[0] === 'undefined')
            $('#wholeauth-root').append($dloader);
    };
    _.lock = function($content) {
        // almacenamos el contenido
        $('.wholeauth-worker .content').html($content !== undefined ? $content : '');
        // mostramos el mensaje de carga
        $('.wholeauth-worker').stop().fadeIn(250);
        // centramos el contenedor
        $('.wholeauth-worker .content').css('margin-top', -($('.wholeauth-worker .content').actual('height') / 2));
        $('.wholeauth-worker .content').css('margin-left', -($('.wholeauth-worker .content').actual('width') / 2));
    };
    _.unlock = function() {
        // mostramos el mensaje de carga
        $('.wholeauth-worker').stop().fadeOut(150);
    };
    _.loading = function(percent) {
        // verificamos si es false
        if (percent == false) {
            // ocultamos el loader
            $('.wholeauth-dataloader').stop().fadeOut(150);
            setTimeout(function() {
                // vaciamos el texto
                $('.wholeauth-dataloader span').text('');
                $('.wholeauth-dataloader').hide();
            }, 150);
        } else {
            // mostramos el loader
            $('.wholeauth-dataloader').fadeIn(250);
            // verificamos si hay porcentaje
            if (percent !== undefined && !isNaN(percent))
                // seteamos el porcentaje
                $('.wholeauth-dataloader span').text(' (' + Math.round(percent) + '%)');
        }
    };
}(jQuery, WholeAuth));