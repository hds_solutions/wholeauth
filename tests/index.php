<!DOCTYPE html>
<html>
    <head>
        <title>WholeAuth Tests</title>
        <link rel="stylesheet" type="text/css" href="css/vendor/normalize.min.css">
        <link rel="stylesheet" type="text/css" href="css/vendor/elegant-icons.min.css">
        <link rel="stylesheet" type="text/css" href="../src/net/hds-solutions/wholeauth/css/alertify.core.css">
        <link rel="stylesheet" type="text/css" href="../src/net/hds-solutions/wholeauth/css/alertify.default.css">
        <link rel="stylesheet" type="text/css" href="../src/net/hds-solutions/wholeauth/css/wholeauth-wk.css">
        <link rel="stylesheet" type="text/css" href="../src/net/hds-solutions/wholeauth/css/wholeauth-forms.css">
    	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    	<script type="text/javascript" src="js/vendor/jquery.actual.min.js"></script>
    	<script type="text/javascript" src="js/vendor/jquery.finger.min.js"></script>
    	<script type="text/javascript" src="js/vendor/jquery.ajax-progress.min.js"></script>
    	<!--
        <script type="text/javascript" src="//apis.hds-solutions.net/wholeauth/wholeauth.min.js"></script>
        <script type="text/javascript" src="//apis.hds-solutions.net/wholeauth/wholeauth-fb.min.js"></script>
        <script type="text/javascript" src="//apis.hds-solutions.net/wholeauth/wholeauth-ga.min.js"></script>
        <script type="text/javascript" src="//apis.hds-solutions.net/wholeauth/wholeauth-wk.min.js"></script>
        -->
        <script type="text/javascript" src="../src/net/hds-solutions/wholeauth/js/wholeauth.js"></script>
        <script type="text/javascript" src="../src/net/hds-solutions/wholeauth/js/wholeauth-fb.js"></script>
        <script type="text/javascript" src="../src/net/hds-solutions/wholeauth/js/wholeauth-ga.js"></script>
        <script type="text/javascript" src="../src/net/hds-solutions/wholeauth/js/wholeauth-wk.js"></script>
        <script type="text/javascript" src="../src/net/hds-solutions/wholeauth/js/wholeauth-forms.js"></script>
        <script type="text/javascript" src="../src/net/hds-solutions/wholeauth/js/wholeauth-alertify.js"></script>
        <script type="text/javascript" src="../src/net/hds-solutions/wholeauth/js/wholeauth-fx.js"></script>
        <script type="text/javascript" src="../src/net/hds-solutions/wholeauth/js/wholeauth-css.js"></script>
    </head>
    <body>
        <div id="parallax" style="width: 500px; height: 200px; left: 550px; top: 300px; background: #add; position: absolute;">
            <div class="attached-wrapper">
                <p>Arrastre aqu&iacute; los archivos a subir</p>
                <span class="icon_upload color-gray"></span>
            </div>
            <div id="uploads"></div>
        </div>
    	<script type="text/javascript">
            $(function() {
                _.fx.parallax('#parallax', 100, 100);
        	   _.fb.init('261701603990673');
        	   _.ga.init('UA-47583042-3', 'gridserver.com');
        	   var txt = '';
               for (var i in _)
                   if (_[i].version !== undefined)
                       txt += i + ': ' + _[i].version + '<br/>';
               $(document.body).append(txt);
               /*
               _.confirm('Access to facebook?', function(r) {
                   if (r) {
                       _.lock('Checking Facebook login..');
                       _.fb.checkLogin(function(r2) {
                           if (!r2) {
                               _.confirm('Access to facebook?', function(r3) {
                                   if (r3) {
                                       _.lock('Login into facebook..');
                                       _.fb.login(function(a) {
                                           if (!a)
                                               _.alert('Facebook login failed');
                                           else
                                               _.alert('Facebook login successfully');
                                           _.unlock();
                                       });
                                   } else
                                       _.alert('Facebook login canceled');
                               });
                           } else {
                               _.alert('Facebook login successfully');
                               _.unlock();
                           }
                       });
                   } else
                       _.prompt('Then, put your username:');
               });
               */
               _.forms.uploader('.attached-wrapper', {
                   onupload: function(id) {
                       _.log(id);
                   }
               });
           });
    	</script>
    </body>
</html>